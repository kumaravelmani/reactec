import { push } from 'react-router-redux';
import { AnyAction, Dispatch } from 'redux';
import { AppState } from '../redux';
import {
  STORE_ERROR_MESSAGE,
  STORE_ERROR_TITLE,
  STORE_ERROR_BAR,
  STORE_SUCCESS_MESSAGE,
  STORE_SUCCESS_TITLE,
  STORE_SUCCESS_BAR,
  STORE_ERROR_FILES_UPLOADED,
  STORE_SUCCESS_FILES_UPLOADED,
  STORE_WARN_FILES_UPLOADED,
  STORE_SUCCESS_SUBMIT,
  STORE_ERROR_SUBMIT,
  STORE_ERROR_UPDATE,
} from '../reducers/errors';
// import { STORE_UPDATED_IMAGE, STORE_UPDATE_ORDER, UpdateOrderStruct, STORE_STATUS } from 'src/reducers/updateOrder';

export const loadErrorPage = (
  errorMessage: string,
  errorTitle: string,
  errorBar: boolean,
  successMessage: string,
  successTitle: string,
  successBar: boolean
) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    dispatch({ type: STORE_ERROR_MESSAGE, errorMessage } as AnyAction);
    dispatch({ type: STORE_ERROR_TITLE, errorTitle } as AnyAction);
    dispatch({ type: STORE_ERROR_BAR, errorBar } as AnyAction);
    dispatch({ type: STORE_SUCCESS_MESSAGE, successMessage } as AnyAction);
    dispatch({ type: STORE_SUCCESS_TITLE, successTitle } as AnyAction);
    dispatch({ type: STORE_SUCCESS_BAR, successBar } as AnyAction);
    dispatch(push('/error'));
    return true;
  };
};

export const handleCloseAlert = (
  dispatch: Dispatch<AppState>,
  getState: () => AppState
) => {
  const errorBar: boolean = false;
  dispatch({ type: STORE_ERROR_BAR, errorBar } as AnyAction);
  const successBar: boolean = false;
  dispatch({ type: STORE_SUCCESS_BAR, successBar } as AnyAction);
};

export const onFilesErrorSmile = (error: Error, images: Array<File>) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    console.error(error.message);
    const errorFileUploadedOpen: boolean = true;
    dispatch({
      type: STORE_ERROR_FILES_UPLOADED,
      errorFileUploadedOpen
    } as AnyAction);
  };
};

export const onFilesErrorStlzip = (error: Error, fileszip: Array<File>) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    console.error(error.message);
    const errorFileUploadedOpen: boolean = true;
    dispatch({
      type: STORE_ERROR_FILES_UPLOADED,
      errorFileUploadedOpen
    } as AnyAction);
  };
};

export const onFileError = (error: Error, file: Array<File>) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const errorFileUploadedOpen: boolean = true;
    dispatch({ type: STORE_ERROR_FILES_UPLOADED, errorFileUploadedOpen } as AnyAction);
  };
};

export const onFileError2 = (error: Error, file: Array<File>) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const errorFileUploadedOpen: boolean = true;
    dispatch({ type: STORE_ERROR_FILES_UPLOADED, errorFileUploadedOpen } as AnyAction);
  };
};

export const onDropNewSmileError = (error: Error, newFile: Array<File>) => {
  return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const errorFileUploadedOpen: boolean = true;
    dispatch({ type: STORE_ERROR_FILES_UPLOADED, errorFileUploadedOpen } as AnyAction);
  };
};

export const closeError = (dispatch: Dispatch<AppState>, getState: () => AppState) => {
  const errorFileUploadedOpen: boolean = false;
  const successFileUploadOpen: boolean = false;
  const warnFileUploadOpen: boolean = false;
  const errorSubmitOpen: boolean = false;
  const successSubmitOpen: boolean = false;
  const errorUpdateOpen: boolean = false;

  dispatch({ type: STORE_ERROR_UPDATE, errorUpdateOpen } as AnyAction);
  dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);
  dispatch({ type: STORE_ERROR_FILES_UPLOADED, errorFileUploadedOpen } as AnyAction);
  dispatch({ type: STORE_WARN_FILES_UPLOADED, warnFileUploadOpen } as AnyAction);
  dispatch({ type: STORE_SUCCESS_SUBMIT, successSubmitOpen } as AnyAction);
  dispatch({ type: STORE_ERROR_SUBMIT, errorSubmitOpen } as AnyAction);

};

export const closeAlertUpdate = (dispatch: Dispatch<AppState>, getState: () => AppState) => {

  // const successUpdateOpen: boolean = false;
  // dispatch({ type: STORE_SUCCESS_UPDATE, successUpdateOpen } as AnyAction);
  // const updatedImage: Array<File> = [];
  // dispatch({ type: STORE_UPDATED_IMAGE, updatedImage } as AnyAction);
  // const updateOrderData: UpdateOrderStruct[] = [];
  // dispatch({ type: STORE_UPDATE_ORDER, updateOrderData } as AnyAction);
  // const status = 0;
  // dispatch({ type: STORE_STATUS, status } as AnyAction);
  location.href = document.referrer;

};
