import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles, Theme, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Card, CardHeader, CardContent, Tooltip, IconButton, Typography } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import { RowData } from 'src/reducers/viewOrder';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { Link } from 'react-router-dom';
// import BeforeAfterSlider from 'react-before-after-slider';
// import { InputLabel } from '@material-ui/core';

export interface ViewOrderViewProps {
    viewOrderData: RowData[];
    onback: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexWrap: 'wrap',
            flexGrow: 1,
            justifyContent: 'space-around',
            // backgroundColor: theme.palette.background.paper,
            height: 'calc(100vh - 13px)',
            overflow: 'auto'
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120
        },
        selectEmpty: {
            marginTop: theme.spacing(1)
        },
        card: {
            maxWidth: 3500,
            marginTop: theme.spacing(2)
        },
        cardHeader: {
            alignItems: 'center'
        },
        paper: {
            marginTop: theme.spacing(1),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            // padding: theme.spacing(2),
            // textAlign: 'center',
            // color: theme.palette.text.secondary,
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main
        },
        form: {
            maxWidth: '100%', // Fix IE 11 issue.
            // marginBottom: theme.spacing(1),
        },
        textarea: {
            height: 10
        },
        forms: {
            width: '100%', // Fix IE 11 issue.
        },
        submit: {
            margin: theme.spacing(3, 0, 2)
        },
        gridList: {
            width: 600,
            height: 200,
        },
        titleBar: {
            background:
                'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
                'rgba(0,0,0,0.1) 70%, rgba(0,0,0,0) 90%)',
        },
        tilebar: {
            justifyContent: 'center',
            alignItems: 'center',
            margin: theme.spacing(1)
        },
        icon: {
            color: 'white',
        },
        imagecenter: {
            top: '50%',
            left: '0',
            width: '100%',
            height: '100%',
            position: 'relative',
            transform: 'translateY(-50%)'
        },
        verticalLine: {
            borderRight: '2px solid black',
            width: '50%',
            height: '185px',
            padding: '5px 10px !important'
        },
        font: {
            fontSize: '1.5rem',
            fontFamily: 'Apple Color Emoji',
            color: '#3f51b5',
            textAlign: 'center',
            margin: '5px 0'
        },
        gridPosition: {
            position: 'relative'
        },
        gridRight: {
            position: 'absolute',
            flexDirection: 'row-reverse',
            height: '100%',
            alignContent: 'center',
            width: '100%'
        }
    }),
);

const CssTextField = withStyles({
    root: {
        '& .MuiInputBase-root.Mui-disabled': {
            color: '#000',
        },
    },
})(TextField);

const ViewOrderView: React.StatelessComponent<ViewOrderViewProps> = props => {
    const classes = useStyles({});

    const status = ['Order Created', 'Design in Progress', 'Design Completed'];
    const servicename = ['None', 'Easy Smile', 'Easy Smile Pro'];

    return (
        <div className={classes.root}>
            <Container component="main" maxWidth="sm">
                <CssBaseline />
                <div className={classes.paper}>
                    <Grid container spacing={3}>
                        <Card className={classes.card}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="recipe" className={classes.avatar}>
                                        <ContactMailIcon />
                                    </Avatar>}
                                action={
                                    <Link to={'javascript:void(0)'} onClick={props.onback}>
                                        <Tooltip title="Back">
                                            <IconButton color="primary" style={{ backgroundColor: 'bisque' }} aria-label="settings" >
                                                <KeyboardBackspaceIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </Link>}
                                title={<h2>View Smile Order</h2>}
                            />
                            {
                                props.viewOrderData !== undefined ?

                                    <CardContent>
                                        <form className={classes.form} noValidate>
                                            <Grid container spacing={1}>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Type of Service"
                                                        disabled
                                                        defaultValue=""
                                                        value={servicename[props.viewOrderData[0].servicename] || ''}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Order No"
                                                        disabled
                                                        defaultValue=""
                                                        value={props.viewOrderData[0].orderno}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Status"
                                                        disabled
                                                        defaultValue=""
                                                        value={status[props.viewOrderData[0].status]}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Patient Name"
                                                        disabled
                                                        value={props.viewOrderData[0].p_name}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={3}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Age"
                                                        disabled
                                                        value={props.viewOrderData[0].p_age}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={3}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Gender"
                                                        disabled
                                                        value={props.viewOrderData[0].p_gender}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </form>
                                        <Typography variant="h3" className={classes.font}>Before | After Preview</Typography>
                                        <div className={classes.gridPosition}>
                                            <GridList spacing={2} className={classes.gridRight}>
                                                <GridListTile style={{ padding: '5px 10px' }}>
                                                    <a href={props.viewOrderData[0].finalsmilefile} target="_blank" download>
                                                        <img src={props.viewOrderData[0].finalsmilefile} alt="" className={classes.imagecenter} />
                                                    </a>
                                                    <GridListTileBar
                                                        title="Designed Smile"
                                                        titlePosition="top"
                                                        actionPosition="left"
                                                        className={classes.titleBar}
                                                    />
                                                </GridListTile>
                                            </GridList>
                                            <GridList spacing={2}>
                                                <GridListTile className={classes.verticalLine}>
                                                    <img src={props.viewOrderData[0].widesmailfile} alt="" />
                                                    <GridListTileBar
                                                        title="Wide Smile"
                                                        titlePosition="top"
                                                        actionPosition="left"
                                                        className={classes.titleBar}
                                                    />
                                                </GridListTile>
                                            </GridList>
                                            <GridList spacing={2}>
                                                <GridListTile className={classes.verticalLine}>
                                                    <img src={props.viewOrderData[0].normalsmilefile} alt="" />
                                                    <GridListTileBar
                                                        title="Normal Smile"
                                                        titlePosition="top"
                                                        actionPosition="left"
                                                        className={classes.titleBar}
                                                    />
                                                </GridListTile>
                                            </GridList>
                                        </div>
                                    </CardContent>
                                    : null}
                        </Card>
                    </Grid>
                </div>
            </Container>
        </div>
    );
};

export default ViewOrderView;
