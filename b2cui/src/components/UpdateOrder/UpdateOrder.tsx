import * as React from 'react';
import UpdateOrderView from '../layouts/UpdateOrderView';
import { Snackbar } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { UpdateOrderStruct } from 'src/reducers/updateOrder';

export interface UpdateOrderProps {
    onComponentDidMount: (orderId: string | null, usercode: string | null) => void;
    updateOrderData: UpdateOrderStruct[];
    updatedImage: Array<File>;
    status: number;

    onDownloadUpdatestatus: () => void;
    onDownloadUpdate: () => void;
    ondownloadImage: () => void;
    onDropNewSmile: (newFile: Array<File>) => void;
    onDropNewSmileError: (error: Error, newFile: Array<File>) => void;
    onSaveStatus: (event: React.ChangeEvent<{ value: number }>) => void;
    onUpdateNewFile: () => void;
    resetFinalfilesmile: () => void;

    // <---------------------------------------------For alert----------------------------------------------------------->
    successFileUploadOpen: boolean;
    successFileUploadTitle: string;
    successfilemsg: string;

    errorfileopen: boolean;
    errorFileUploadTitle: string;
    errorfilemsg: string;

    warningSubmitOpen: boolean;
    successUpdateOpen: boolean;
    successUpdatemsg: string;

    errorUpdateopen: boolean;
    errorUpdatemsg: string;

    closeAlertUpdate: () => void;
    oncloseError: () => void;

}

class UpdateOrder extends React.Component<UpdateOrderProps> {
    render() {
        return (
            <div>
                <UpdateOrderView
                    updateOrderData={this.props.updateOrderData}
                    onDropNewSmile={this.props.onDropNewSmile}
                    onDropNewSmileError={this.props.onDropNewSmileError}
                    onUpdateNewFile={this.props.onUpdateNewFile}
                    onSaveStatus={this.props.onSaveStatus}
                    updatedImage={this.props.updatedImage}
                    resetFinalfilesmile={this.props.resetFinalfilesmile}
                    status={this.props.status}
                    onDownloadUpdatestatus={this.props.onDownloadUpdatestatus}
                    onDownloadUpdate={this.props.onDownloadUpdate}
                />
                {/* <----------------------------------------File drag and drop (Error)--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    transitionDuration={1000}
                    open={this.props.errorfileopen}
                    autoHideDuration={3000}
                    onClose={this.props.oncloseError}
                >
                    <Alert severity="error" onClose={this.props.oncloseError}>
                        <AlertTitle>{this.props.errorFileUploadTitle}</AlertTitle>
                        {this.props.errorfilemsg}
                    </Alert>
                </Snackbar>
                {/* <----------------------------------------File drag and drop (Success)--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.successFileUploadOpen}
                    autoHideDuration={3000}
                    onClose={this.props.oncloseError}
                >
                    <Alert severity="success" onClose={this.props.oncloseError}>
                        <AlertTitle>{this.props.successFileUploadTitle}</AlertTitle>
                        {this.props.successfilemsg}
                    </Alert>
                </Snackbar>
                {/* <----------------------------------------File drag and drop (Warning)--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.errorUpdateopen}
                    autoHideDuration={6000}
                    onClose={this.props.oncloseError}
                >
                    <Alert severity="error" onClose={this.props.oncloseError}>
                        <AlertTitle>Error</AlertTitle>
                        {this.props.errorUpdatemsg}
                    </Alert>
                </Snackbar>
                {/* <------------------------------------------------------Update Successfully---------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.successUpdateOpen}
                    onClose={this.props.closeAlertUpdate}
                >
                    <Alert severity="success" onClose={this.props.closeAlertUpdate}>
                        <AlertTitle>Success</AlertTitle>
                        {this.props.successUpdatemsg}
                    </Alert>
                </Snackbar>
            </div>

        );
    }
    componentDidMount() {
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const usercode: string | null = urlParams.get('userCode');
        const orderId: string | null = urlParams.get('orderId');
        this.props.onComponentDidMount(orderId, usercode);
    }
}

export default UpdateOrder;
