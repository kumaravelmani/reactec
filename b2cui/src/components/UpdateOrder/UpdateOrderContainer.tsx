import { connect } from 'react-redux';
import UpdateOrder from './UpdateOrder';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';

import { onDropNewSmileError, closeAlertUpdate, closeError } from 'src/actions/errorAction';
// import { onSaveuserCode } from 'src/actions/orderAction';
import { getUpdateOrderData, onDropNewSmile, UpdateNewFile, resetFinalfiles, onSaveStatusData, ondownloadUpdatestatus, onDownloadUpdate } from 'src/actions/updateorderAction';

export const mapStateToProps = (state: AppState) => {
    return {
        updateOrderData: state.updateOrder.updateOrderData,
        updatedImage: state.updateOrder.updatedImage,
        status: state.updateOrder.status,

        successFileUploadOpen: state.errorState.successFileUploadOpen,
        successFileUploadTitle: state.errorState.successFileUploadTitle,
        successfilemsg: state.errorState.successfilemsg,
        errorfileopen: state.errorState.errorFileUploadedOpen,
        errorFileUploadTitle: state.errorState.errorFileUploadTitle,
        errorfilemsg: state.errorState.errorfilemsg,

        successUpdateOpen: state.errorState.successUpdateOpen,
        successUpdatemsg: state.errorState.successUpdatemsg,

        errorUpdateopen: state.errorState.errorUpdateOpen,
        errorUpdatemsg: state.errorState.errorUpdatemsg,
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onComponentDidMount: (orderId: string | null, usercode: string | null) => {
            // dispatch(onSaveOrderno(orderno));
            // dispatch(onSaveuserCode(usercode));
            dispatch(getUpdateOrderData(orderId, usercode));
            dispatch(resetFinalfiles);

        },
        closeAlertUpdate: () => {
            dispatch(closeAlertUpdate);
        },
        oncloseError: () => {
            dispatch(closeError);
        },
        onDropNewSmile: (newFile: Array<File>) => {
            dispatch(onDropNewSmile(newFile));
        },
        onDropNewSmileError: (error: Error, newFile: Array<File>) => {
            dispatch(onDropNewSmileError(error, newFile));
        },
        onSaveStatus: (event: React.ChangeEvent<{ value: number }>) => {
            dispatch(onSaveStatusData(event));
        },
        onUpdateNewFile: () => {
            dispatch(UpdateNewFile);
        },
        resetFinalfilesmile: () => {
            dispatch(resetFinalfiles);
        },
        onDownloadUpdatestatus: () => {
            dispatch(ondownloadUpdatestatus);
        },
        onDownloadUpdate: () => {
            dispatch(onDownloadUpdate);
        },
    };
};

const UpdateOrderContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(UpdateOrder);

export default UpdateOrderContainer;
