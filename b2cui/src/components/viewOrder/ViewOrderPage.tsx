import * as React from 'react';
import ViewOrderView from '../layouts/ViewOrder';
import { RowData } from 'src/reducers/viewOrder';
// import { RowData } from 'src/reducers/draftorderfile';
// import { UserDetail } from '../../reducers/user';

export interface LiveOrderPageViewProps {
    onComponentDidMount: (orderId: string | null, usercode: string | null) => void;
    viewOrderData: RowData[];
    onback: () => void;
}

class ViewOrderPage extends React.Component<LiveOrderPageViewProps> {
    render() {
        return (
            <ViewOrderView viewOrderData={this.props.viewOrderData} onback={this.props.onback} />
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const orderId: string | null = urlParams.get('orderId');
        const usercode: string | null = urlParams.get('userCode');
        this.props.onComponentDidMount(orderId, usercode);
        // this.props.onComponentDidMount();
    }
}

export default ViewOrderPage;
