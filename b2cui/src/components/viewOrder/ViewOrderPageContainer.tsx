import { connect } from 'react-redux';
import ViewOrderPage from './ViewOrderPage';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import { getViewOrderData, onBack } from 'src/actions/viewOrderAction';

export const mapStateToProps = (state: AppState) => {
    return {
        viewOrderData: state.viewOrder.viewOrderData,
        
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onComponentDidMount: (orderId: string | null, usercode: string | null) => {
            dispatch(getViewOrderData(orderId, usercode));
        },
        onback: () => {
            dispatch(onBack);
        }
    };
};

const ViewOrderPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ViewOrderPage);

export default ViewOrderPageContainer;
