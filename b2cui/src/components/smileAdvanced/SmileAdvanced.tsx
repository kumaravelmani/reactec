import * as React from 'react';
import SmileAdvancedView from '../layouts/SmileAdvancedView';
import { Snackbar } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

export interface SmileAdvancedProps {
    onComponentDidMount: (query: string | null) => void;
    AdvancedFileType: () => void;
    cameraDisplayOptions: string;
    photoDisplayOption: string;
    smileDisplayOptions: string;
    advancedDisplayOption: string;
    handleWideTakePhoto: (dataUri: string) => void;
    handleSmileTakePhoto: (dataUri: string) => void;
    onFilesChangeNormalSmile: (images: Array<File>) => void;
    onFilesChangeWideSmile: (images: Array<File>) => void;
    onFilesErrorSmile: (error: Error, images: Array<File>) => void;
    onFilesChangeStlzip: (fileszip: Array<File>) => void;
    onFilesErrorStlzip: (error: Error, filezip: Array<File>) => void;
    imagesNormal: Array<File>;
    imagesWide: Array<File>;
    filezip: Array<File>;
    imagedatanormal: Array<File>;
    imagedatawide: Array<File>;
    onUploadImages: () => void;
    onUploadzip: () => void;
    resetFileValidateb2c: () => void;
    resetFileSmileb2c: () => void;
    createOrder: () => void;

    successFileUploadOpen: boolean;
    successFileUploadTitle: string;
    successfilemsg: string;
    errorfileopen: boolean;
    errorFileUploadTitle: string;
    errorfilemsg: string;
    warningSubmitOpen: boolean;
    successSubmitOpen: boolean;
    successSubmitmsg: string;

    errorSubmitopen: boolean;
    errorSubmitmsg: string;
    closeErrorfileUpload: () => void;

    onSavePFirstName: (pfirstname: string) => void;
    onSavePLastName: (plastname: string) => void;
    onSavePAge: (page: string) => void;
    onSavePGender: (pgender: string) => void;

    pfirstname: string;
    plastname: string;
    page: string;
    pgender: string;
}

class SmileAdvanced extends React.Component<SmileAdvancedProps> {
    render() {
        return (
            <div>
                <SmileAdvancedView
                    AdvancedFileType={this.props.AdvancedFileType}
                    cameraDisplayOptions={this.props.cameraDisplayOptions}
                    photoDisplayOption={this.props.photoDisplayOption}
                    smileDisplayOptions={this.props.smileDisplayOptions}
                    advancedDisplayOption={this.props.advancedDisplayOption}
                    handleSmileTakePhoto={this.props.handleSmileTakePhoto}
                    handleWideTakePhoto={this.props.handleWideTakePhoto}
                    onFilesChangeNormalSmile={this.props.onFilesChangeNormalSmile}
                    onFilesChangeWideSmile={this.props.onFilesChangeWideSmile}
                    onFilesErrorSmile={this.props.onFilesErrorSmile}
                    onFilesChangeStlzip={this.props.onFilesChangeStlzip}
                    onFilesErrorStlzip={this.props.onFilesErrorStlzip}
                    imagesNormal={this.props.imagesNormal}
                    imagesWide={this.props.imagesWide}
                    filezip={this.props.filezip}
                    imagedatanormal={this.props.imagedatanormal}
                    imagedatawide={this.props.imagedatawide}
                    onUploadImages={this.props.onUploadImages}
                    onUploadzip={this.props.onUploadzip}
                    resetFileValidateb2c={this.props.resetFileValidateb2c}
                    resetFileSmileb2c={this.props.resetFileSmileb2c}
                    createOrder={this.props.createOrder}
                    onSavePFirstName={this.props.onSavePFirstName}
                    onSavePLastName={this.props.onSavePLastName}
                    onSavePAge={this.props.onSavePAge}
                    onSavePGender={this.props.onSavePGender}
                    pfirstname={this.props.pfirstname}
                    plastname={this.props.plastname}
                    page={this.props.page}
                    pgender={this.props.pgender}
                />
                {/* <----------------------------------------File drag and drop (Error)--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    transitionDuration={1000}
                    open={this.props.errorfileopen}
                    autoHideDuration={3000}
                    onClose={this.props.closeErrorfileUpload}
                >
                    <Alert severity="error" onClose={this.props.closeErrorfileUpload}>
                        <AlertTitle>{this.props.errorFileUploadTitle}</AlertTitle>
                        {this.props.errorfilemsg}
                    </Alert>
                </Snackbar>
                {/* <----------------------------------------File drag and drop (Success)--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.successFileUploadOpen}
                    autoHideDuration={3000}
                    onClose={this.props.closeErrorfileUpload}
                >
                    <Alert severity="success" onClose={this.props.closeErrorfileUpload}>
                        <AlertTitle>{this.props.successFileUploadTitle}</AlertTitle>
                        {this.props.successfilemsg}
                    </Alert>
                </Snackbar>
                {/* <----------------------------------------Submit error--------------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.errorSubmitopen}
                    autoHideDuration={4000}
                    onClose={this.props.closeErrorfileUpload}
                >
                    <Alert severity="error" onClose={this.props.closeErrorfileUpload}>
                        <AlertTitle>Error</AlertTitle>
                        {this.props.errorSubmitmsg}
                    </Alert>
                </Snackbar>
                {/* <------------------------------------------------------Submit Successfully---------------------------------> */}
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    transitionDuration={1000}
                    open={this.props.successSubmitOpen}
                    autoHideDuration={4000}
                    onClose={this.props.closeErrorfileUpload}
                >
                    <Alert severity="success" onClose={this.props.closeErrorfileUpload}>
                        <AlertTitle>Success</AlertTitle>
                        {this.props.successSubmitmsg}
                    </Alert>
                </Snackbar>
            </div>

        );
    }
    componentDidMount() {
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const query: string | null = urlParams.get('userCode');
        this.props.onComponentDidMount(query);
    }
}

export default SmileAdvanced;
