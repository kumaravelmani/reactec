import { AnyAction, Reducer } from 'redux';

export const STORE_UPDATE_ORDER = 'updateOrder/Order-details';
export const STORE_ORDERID = 'orderId/orderId-data';
export const STORE_UPDATED_IMAGE = 'image/updated-image';
export const STORE_STATUS = 'status/updated-order';

export const STORE_USERCODE = 'usercode/updateorder';

export interface UpdateOrderStruct {
    rowno: string;
    orderno: string;
    orderdate: string;
    filename: string;
    filesize: string;
    material: string;
    servicename: string;
    designtype: string;
    tat: string;
    msg: string;
    nounit: string;
    status: number;
    normal_smile_url: string;
    wide_smile_url: string;
    final_smile_url: string;
    ds_ordersid: number;
    qc_rating: string;
    qc_remarks: string;
    normalsmilefile: string;
    widesmailfile: string;
    finalsmilefile: string;
    p_name: string;
    p_age: string;
    p_gender: string;
}

export interface UpdateOrderState {
    updateOrderData: UpdateOrderStruct[];
    orderId: string;
    updatedImage: Array<File>;
    status: number;
    usercode: string;
}

const initialState: UpdateOrderState = {
    updateOrderData: [{
        rowno: '',
        orderno: '',
        orderdate: '',
        filename: '',
        filesize: '',
        material: '',
        servicename: '',
        designtype: '',
        tat: '',
        msg: '',
        nounit: '',
        status: 0,
        normal_smile_url: '',
        wide_smile_url: '',
        final_smile_url: '',
        ds_ordersid: 0,
        qc_rating: '',
        qc_remarks: '',
        normalsmilefile: '',
        widesmailfile: '',
        finalsmilefile: '',
        p_name: '',
        p_age: '',
        p_gender: ''
    }],
    updatedImage: [],
    orderId: '',
    status: 0,
    usercode: ''
};

const reducer: Reducer<UpdateOrderState> = (state: UpdateOrderState = initialState, action: AnyAction): UpdateOrderState => {
    switch (action.type) {
        case STORE_UPDATE_ORDER:
            return {
                ...state,
                updateOrderData: action.updateOrderData,
            };
        case STORE_UPDATED_IMAGE:
            return {
                ...state,
                updatedImage: action.updatedImage,
            };
        case STORE_ORDERID:
            return {
                ...state,
                orderId: action.orderId
            };
        case STORE_STATUS:
            return {
                ...state,
                status: action.status
            };
        case STORE_USERCODE:
            return {
                ...state,
                usercode: action.usercode
            };
        default:
            return state;
    }
};

export default reducer;
