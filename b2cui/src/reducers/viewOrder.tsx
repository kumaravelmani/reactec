import { AnyAction, Reducer } from 'redux';

// export const STORE_DRAFTORDER = 'file/files-upload';
export const STORE_VIEWORDERDATA = 'data/view-order';
export const STORE_ORDERID = 'orderid/order-number';

export const STORE_USERCODE = 'usercode/vieworder';

export interface RowData {
    rowno: string;
    orderno: string;
    orderdate: string;
    filename: string;
    filesize: string;
    material: string;
    servicename: string;
    designtype: string;
    tat: string;
    msg: string;
    nounit: string;
    status: number;
    normal_smile_url: string;
    wide_smile_url: string;
    final_smile_url: string;
    ds_ordersid: number;
    normalsmilefile: string;
    widesmailfile: string;
    finalsmilefile: string;
    p_name: string;
    p_age: string;
    p_gender: string;
}

export interface ViewOrderState {
    // files: Array<File>;
    viewOrderData: RowData[];
    orderId: string;
    usercode: string;
}

const initialState: ViewOrderState = {
    // files: [],
    viewOrderData: [{
        rowno: '',
        orderno: '',
        orderdate: '',
        filename: '',
        filesize: '',
        material: '',
        servicename: '',
        designtype: '',
        tat: '',
        msg: '',
        nounit: '',
        status: 0,
        normal_smile_url: '',
        wide_smile_url: '',
        final_smile_url: '',
        ds_ordersid: 0,
        normalsmilefile: '',
        widesmailfile: '',
        finalsmilefile: '',
        p_name: '',
        p_age: '',
        p_gender: ''
    }],
    orderId: '',
    usercode: ''
};

const reducer: Reducer<ViewOrderState> = (
    state: ViewOrderState = initialState,
    action: AnyAction
): ViewOrderState => {
    switch (action.type) {
        case STORE_VIEWORDERDATA:
            return {
                ...state,
                viewOrderData: action.viewData
            };
        case STORE_ORDERID:
            return {
                ...state,
                orderId: action.orderId
            };
        case STORE_USERCODE:
            return {
                ...state,
                usercode: action.usercode
            };
        default:
            return state;
    }
};

export default reducer;
