import { AnyAction, Reducer } from 'redux';

export const STORE_EID_NUMBER = 'basic/store-eid-number';
export const STORE_ICCID_NUMBER = 'basic/store-iccid-number';
export const CIP_ACCESS_TOKEN = 'basic/cip-access-token';
export const CIP_TOKEN_TYPE = 'basic/cip-token-type';
export const CIP_EXPIRES_IN = 'basic/cip-expires-in';
export const CIP_GUID = 'basic/cip-guid';
export const STORE_ICCID_NUMBERS = 'basic/store-iccid-numbers';
export const IS_MULTIPLE_ICCID = 'basic/is-multiple-iccids';

export interface BasicState {
    eidNumber?: string;
    iccidNumber?: string;
    cipAccessToken?: string;
    cipTokenType?: string;
    cipExpiresIn?: string;
    cipGuid?: string;
    iccids?: string[];
    isMultipleIccids?: boolean;
}

const initialState: BasicState = {
    isMultipleIccids: false
};

const reducer: Reducer<BasicState> =
    (state: BasicState = initialState, action: AnyAction): BasicState => {
        switch (action.type) {
            case STORE_EID_NUMBER:
                return {
                    ...state,
                    eidNumber: action.eidNumber,
                };
            case STORE_ICCID_NUMBER:
                return {
                    ...state,
                    iccidNumber: action.iccidNumber
                };
            case CIP_ACCESS_TOKEN:
                return {
                    ...state,
                    cipAccessToken: action.cipAccessToken
                };
            case CIP_TOKEN_TYPE:
                return {
                    ...state,
                    cipTokenType: action.cipTokenType
                };
            case CIP_EXPIRES_IN:
                return {
                    ...state,
                    cipExpiresIn: action.cipExpiresIn
                };
            case CIP_GUID:
                return {
                    ...state,
                    cipGuid: action.cipGuid
                };
            case STORE_ICCID_NUMBERS:
                return {
                    ...state,
                    iccids: action.iccids
                };
            case IS_MULTIPLE_ICCID:
                return {
                    ...state,
                    isMultipleIccids: action.isMultipleIccids
                };
            default:
                return state;
        }
    };
export default reducer;
