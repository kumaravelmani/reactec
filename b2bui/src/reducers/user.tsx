import { AnyAction, Reducer } from 'redux';

export const SUCCEED_FETCH_USER = 'user/succeed-fetch-user';
export const SELECT_USER_PROFILE = 'plan/select-user-profile';

export interface UserDetail {
    userName: string;
    esimServiceNumber: string;
    iccid: string;
    esimProfiles?: ESimProfiles[];
}

export interface ESimProfiles {
    esimServiceNumber: string;
    iccid: string;
    billingAccountNumber: string;
    dataPlan: string;
    isInstalled: boolean;
}

export interface UserState {
    userDetails?: UserDetail;
    selectedEsimProfiles?: ESimProfiles;
}

const initialState: UserState = {
    userDetails: {
        esimProfiles: [],
        esimServiceNumber: '',
        iccid: '',
        userName: ''
    }
};

const reducer: Reducer<UserState> = (state: UserState = initialState, action: AnyAction): UserState => {
    switch (action.type) {
        case SUCCEED_FETCH_USER:
            return {
                ...state,
                userDetails: action.userDetails,
            };
        case SELECT_USER_PROFILE:
            return {
                ...state,
                selectedEsimProfiles: action.selectedEsimProfiles
            };
        default:
            return state;
    }
};

export default reducer;
