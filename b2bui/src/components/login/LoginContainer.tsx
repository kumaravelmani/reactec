import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import LoginPage from './LoginPage';
import { push } from 'react-router-redux';

export const mapStateToProps = (state: AppState) => {
    return {
        btnElement: 'Sign-in to Troubleshoot',
        btnType: 'signin'
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: () => {
            // dispatch(fetchCurrentUser);
            dispatch(push('/app'));
        },
        onComponentDidMount:  (url: string) => {
            // dispatch(initiateLogin);
        }
    };
};

const LoginPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoginPage);

export default LoginPageContainer;
