import * as React from 'react';

export interface NewOrderPageProps {
    onComponentDidMount: (url: string ) => void;
    onSubmit: () => void;
    btnElement: string;
    btnType: string;
}

class NewOrderPage extends React.Component<NewOrderPageProps> {
    render() {
        return (
            <div>
                New Order Content will load here
                <button onClick={this.props.onSubmit}>Go back</button>
            </div>
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
    }
}

export default NewOrderPage;
