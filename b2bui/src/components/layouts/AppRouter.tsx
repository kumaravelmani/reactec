import * as React from 'react';
import { ReactNode } from 'react';
import { Route } from 'react-router-dom';
import DashboardPageContainer from '../dashboard/DashboardPageContainer';

class AppRouter extends React.Component {
    render(): ReactNode {

        return (
                <Route path="/home" component={DashboardPageContainer}/>
        );
    }
}

export default AppRouter;
