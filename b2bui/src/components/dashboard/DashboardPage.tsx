import * as React from 'react';

export interface DashboardPageProps {
    onComponentDidMount: (url: string ) => void;
    onSubmit: () => void;
    btnElement: string;
    btnType: string;
}

class DashboardPage extends React.Component<DashboardPageProps> {
    render() {
        return (
            <div>
                Dashboard Content will load here
                <button onClick={this.props.onSubmit}>Next</button>
            </div>
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
    }
}

export default DashboardPage;
