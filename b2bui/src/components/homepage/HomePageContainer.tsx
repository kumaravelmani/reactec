import { connect } from 'react-redux';
import HomePage from './HomePage';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';

export const mapStateToProps = (state: AppState) => {
    return {
        btnElement: 'Sign-in to Troubleshoot',
        btnType: 'signin',
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: () => {
            // dispatch(fetchCurrentUser);
        },
        onComponentDidMount:  (url: string) => {
            // dispatch(initiateLogin);
        }
    };
};

const HomePageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(HomePage);

export default HomePageContainer;
