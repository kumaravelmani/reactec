import * as React from 'react';
import LandingHomePage from '../layouts/LandingHomePage';

export interface HomePageProps {
    onComponentDidMount: (url: string ) => void;
    onSubmit: () => void;
    btnElement: string;
    btnType: string;
}

class HomePage extends React.Component<HomePageProps> {
    render() {
        return (
            <LandingHomePage btnElement={'Test'}/>
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
    }
}

export default HomePage;
