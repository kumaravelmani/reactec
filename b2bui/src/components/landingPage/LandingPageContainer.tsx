import { connect } from 'react-redux';
import LandingPage from './LandingPage';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';

export const mapStateToProps = (state: AppState) => {
    return {
        btnElement: 'Sign-in to Troubleshoot',
        btnType: 'signin',
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: () => {
            // dispatch(fetchCurrentUser);
        },
        onComponentDidMount:  () => {
            // dispatch(initiateLogin);
            // dispatch(push('/app'));
        }
    };
};

const LandingPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LandingPage);

export default LandingPageContainer;
