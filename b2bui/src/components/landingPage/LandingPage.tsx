import * as React from 'react';
import LandingHomePage from '../layouts/LandingHomePage';

export interface LandingPageProps {
    onComponentDidMount: () => void;
    onSubmit: () => void;
    btnElement: string;
    btnType: string;
}

class LandingPage extends React.Component<LandingPageProps> {
    render() {
        return (
            <LandingHomePage btnElement={'Test'}/>
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
        this.props.onComponentDidMount();
    }
}

export default LandingPage;
